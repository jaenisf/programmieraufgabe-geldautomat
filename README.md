# Programmieraufgabe-Geldautomat

Formatierungsfehler im Eclipse Markdown Editor, zum Betrachten der Datei werden andere Markdown-Anzeigen empfohlen, z.B. die GitLab-Weboberfläche.

## Probleme beim ersten Starten (1)

### Umlaute

Umlaute konnten aufgrund von Codierungsproblemen nicht richtig verwendet werden und wurden deswegen im Code angepasst.

* * *

## Sollanforderungen, Einhaltung bzw. Abweichungen (2 u. 3)

### Legende

- [x] Eingehaltene Anforderungen sind mit abgehakten Checkboxen markiert.
- [ ] Abweichungen werden mit nicht abgehakten Checkboxen markiert und
- ***mit einem dicken, kursiven Text, welcher die Abweichung beschreibt unterschrieben*** 

 * * *

### Anwendung

- [x] Steuert den Geldautomaten 
- [x] Textbasierte Konsolenanwendung
- [ ] Bietet dem Benutzer alle Möglichkeiten Geld abzuheben
- ***Fehleingaben führen zu einem Abbruch des Programms in bestimmten Fällen***

* * *

### Geldautomat

- [x] Implementiert von der Klasse Anwendung gesteuerte Funktionalitäten


* * *

### Zustände des Geldautomats

###### Kein Geld

- [x] Startet im Zustand "kein Geld"
- [ ] Wechselt durch das Bestücken vom Zustand "kein Geld" zum Zustand "Bereit"
- ***Der Automat kann auch ohne Geld eine Karte einlesen und Geld ausgeben, allerdings tut er das ohne Kommandozeilenausgaben.***
- [x] Bestückung nur möglich, wenn keine Karte im Automaten ist
- [ ] Fehlermeldung, wenn eine Karte in einen leeren Automaten eingeschoben wird und die anschließende Ausgabe der Karte
- ***Die Karte kann auch, wenn der Automat kein Geld besitzt, eingeschoben werden.***
    
###### Bereit
- [x] Der Geldautomat kann im Zustand "Bereit" eine Karte entgegennehmen und einlesen. Er wechselt dann in den Zustand Karte drin.

###### Karte drin

- [ ] Beim Zustand "Karte drin" muss die passende PIN eingegeben werden
- ***Geld Ausgabe auch ohne passende PIN möglich, siehe PIN korrekt (erster Punkt).***
- [x] Bei korrekter Eingabe der PIN erfolgt der Wechsel vom Zustand "Karte drin" zu "PIN korrekt" 

###### PIN korrekt

- [ ] Darf nur bei korrekter PIN-Eingabe Geld ausgeben
- ***Der Automat gibt aktuell auch Geld aus ohne PIN Eingabe, solange der auszugebender Wert über dem Geldbetrag ist, der im Geldautomaten gespeichert ist und solange der auszugebende Betrag zwischen 5 und 500 Talern liegt. Dabei kommt es zu keiner Meldung über Kommandozeilenausgaben.***
- [x] Im Zustand "PIN korrekt" kann so lange Geld ausgegeben werden, solange der Automat Bargeldreserven hat.
    - [x] Nach dem Ausschöpfen der Bargeldreserven wird der Zustand von "PIN korrekt" zum Zustand "Kein Geld" gewechselt
    - [x] Wenn mehr Geld angefordert wird, als der Automat besitzt, wird ausgegeben, was er noch hat.
    - ***Der Automat gibt sein Restgeld aus, wenn mehr Geld angefordert wird, als er besitzt. Allerdings gibt er dabei keine Kommandozeilenausgabe als Bestätigung aus.***
- Der Auszahlungsbetrag:
    - [x] muss zwischen 5 und 500 Taler liegen
    - [ ] muss durch 5 Teilbar sein
    - ***Es ist möglich Beträge die nicht durch 5 Teilbar sind abzuheben.***
    
* * *

### Karte

- [x] Symbolisiert ein Konto
- [x] Besitzt die Eigenschaft PIN:
    - [x] Wird beim Anlegen einer Karte gesetzt und kann danach nicht geändert werden.
    - [x] Eine vierstellige Zahlenkombination, wobei führende Nullen möglich sein müssen
    - ***Funktion erzeugePin() kann keine führende Nullen erzeugen, allerdings kann die Klasse Karte eine Pin mit führenden Nullen speichern.***
- [x] Funktion zum Abgleichen der im Automaten eingegebenen PIN mit der PIN aus der Karte
- ***Nach dem Abgleichen der Pin wurde der Erfolg bzw. Misserfolg nicht automatisch ausgegeben, sondern musste über die Info-Ausgabefunktion abgefragt werden.***
* * *

### Weiterentwicklung

- [x] (bereits Implementiert) Für die korrekte PIN Eingabe soll es nur noch drei Versuche geben
- Nach drei erfolglosen Versuchen:
    - [x] (bereits Implementiert) soll die Karte ausgeworfen werden,
    - [x] (bereits Implementiert) der Vorgang beendet,
    - [x] (bereits Implementiert) der Zustand von "Karte drin" zu "Bereit" gewechselt werden,
    - [ ] (Implementierung fehlt noch) und der Benutzer über den Abbruch des Vorgangs informiert werden (Ausgabe einer Fehlermeldung und der Aufforderung, die Karte zu entnehmen).
    
## Überprüfung der bestehenden Testabdeckung (4)

### Legende

- [x] Geteste Funktion war schon im Programm vorhanden.
- [ ] Funktion war noch nicht gegeben und musste implementiert werden.

 * * *

### Vorhandene Test

- Auf den ersten Blick laufen alle vorhandenen Unit Tests durch.
- Allerdings wird der Test "testKarteIstNull()" aufgrund der @Ignore-Annotation ignoriert.
    - Nachdem Entfernen der @Ignore-Annotation wirft der Test einen Fehler aus.
    - Der Test wird mit einem "fail("Expected IllegalArgumentException!");" als fehlgeschlagen markiert, hierbei lässt sich vermuten, dass die Programmierer den Fehler entdeckt haben, aber ihn nicht gelöst haben und mit der @Ignore-Annotation ausgeblendet haben.
    - Der Test wirft einen Fehler aus, da eine IllegalStateException erwartet wird, die aber vom zugrundeliegenden Code nicht ausgeworfen wird.
    - Über den "fail("Expected IllegalArgumentException!");" konnten wir herausfinden, dass eigentlich keine IllegalStateException sondern eine IllegalArgumentException erwartet werden sollte.
    - Der Fehler wurde korrigiert, indem in der Funktion "einschieben(Karte karte)" vom Geldautomaten eine Abfrage hinzugefügt wurde, die kontrolliert, ob die übergebene Karte gleich null ist und wenn dieser Fall eintritt, eine IllegalArgumentException ausgibt.

- [ ] testKarteIstNull()    
- [x] testZweiteKarteEinschieben()
- [x] testFalschePin()
- [x] testAbhebungOhnePin()
- [x] testBestueckung()
- [x] testZuWenigGeldImAutomat()
    - Ergänzung: Eingabe der PIN
    
## Ergänzung der Testabdeckung in Bezug auf alle Anforderungen (5)

### Legende

- [x] Geteste Funktion war schon im Programm vorhanden.
- [ ] Funktion war noch nicht gegeben und musste implementiert werden.

 * * *
### Ergänzende Test
 
- [x] testGeldAutomatStartetOhneGeld()
- [ ] testKarteEinschiebenInLeerenAutomaten()
- [x] testBestückungMitEingeschobenerKarte()
- [ ] testAbhebungOhnePinMitMehrGeldAlsVorhanden()
- [x] testZustandswechselBeiKorrekterPinEingabe()
- [x] testAbhebenVonZuVielGeld()
- [x] testAbhebenVonZuWenigGeld()
- [ ] testAbhebenVonNichtDurch5TeilbarenBeträgen()
- [x] testAbhebenVonGeld()
- [x] testPinKorrekt()
- [x] testPinNichtKorrekt()
- [x] testPinKorrektMitFührendenNullen()
- [ ] testPinEingabeOhneVorhandeneKarte()

## Refactoring (6)

- State-Pattern:
	- Erweiterbarkeit und Änderungsstabilität ist gegeben, da neue Zustände einfach ins System integriert werden können, ohne dabei bestehenden Quellcode ändern zu müssen, da eine Änderung bezüglich dem zustandsabhängigen Verhalten lediglich eine Zustandsklasse und nicht den Context betriftt.
	- Intuitivität und Verständlichkei --> Abbildung des Zustandsdiagramms

- Implementierung des Zustandsdiagramms mit dem State-Pattern in Kombination mit dem Singleton-Pattern:
    - Definition der Zustandsübergänge:
        > Es gibt 2 Möglichkeiten (Zustände bestimmen automatisch ihren Folgezustand oder der Folgezustand wird im Kontextobjekt gesetzt).
        - Bei unserer Implementierung bestimmen die Zustände automatisch ihren Folgezustand und weisen diesen Folgezustand als Rückgabewert über die ausfuehren()-Methode dem Kontextobjekt zu. Dadurch kann jeder Zustand seinen Folgezustand selbst bestimmen und dadurch wird der Context von dieser Logik entbunden. Dieser Ansatz funktioniert allerdings nur, da die ausfuehren()-Methode aus unserer Programmlogik heraus keinen anderen Rückgabewert hat.
    - Implementierung der Zustände:
        - Zudem haben wir die Zustände als Singletons realisiert. Dies ist aber nur möglich, da die Zustandsobjekte über keine Attribute und Daten verfügen. Durch diese Vorgehensweise wird der Entwurf sehr flexibel und dynamisch. Außerdem können Zustandswechsel beliebig durchgeführt werden und der Context ist von dieser Verantwortung befreit. Somit können Änderungen an den Zustandsübergängen durchgeführt werden, ohne den Context modifzieren zu müssen.
        - Bei der Implementierung der Singletons haben wir uns für die Version mit Eager Loading entschieden. Bei Eager Loading erfolgt die Instanziierung während die Klasse geladen wird. Ein großer Vorteil dieser Variante ist, neben der Einfachheit, die Threadsicherheit und die Performance. Diese Art und Weise ist jedoch für uns nur sinnvoll, da wir eine geringe Anzahl an Zuständen haben und diese auch alle regelmäßig in der Anwendung benötigt werden.

- Kapselung von Funktionen durch die Einführung von Zuständen (inkl. "Zustände bestimmen Folgezustand" klar definiert) und damit einhergehend eine Reduktion von auftretendem Fehlverhalten

- Alternative Möglichkeit der Implementierung (Siehe [alternativerAnsatz](https://gitlab.com/jaenisf/programmieraufgabe-geldautomat/-/tree/alternativerAnsatz) Branch):
	- alle Funktionen stehen stets zur Verfügung, wobei dafür in jedem Zustand für die nicht aufzurufende Funktion ein Fehlerhandling implementiert werden muss
	- einfachere Art und Weise der Implementierung, jedoch fehleranfälliger und fehlende intutive Nutzbarkeit für den Anwender
	
- zwei Test-Klassen - eine ehemalige und eine aktuelle Test-Klasse für den Geldautomaten
    - bei der neuen Version werden zwei Tests nicht benötigt, da diese durch die Programmlogik nicht mehr aufrufbar sind und durch andere Test abgedeckt werden
    
## Weiterentwicklung (7)

- [x] Eingehaltene Anforderungen sind mit abgehakten Checkboxen markiert.
- [ ] Abweichungen werden mit nicht abgehakten Checkboxen markiert und
- ***mit einem dicken, kursiven Text, welcher die Abweichung beschreibt unterschrieben*** 

 * * *

- [x] (bereits Implementiert) Für die korrekte PIN Eingabe soll es nur noch drei Versuche geben
- Nach drei erfolglosen Versuchen:
    - [x] (bereits Implementiert) soll die Karte ausgeworfen werden,
    - [x] (bereits Implementiert) der Vorgang beendet,
    - [x] (bereits Implementiert) der Zustand von "Karte drin" zu "Bereit" gewechselt werden,
    - [ ] (Implementierung fehlt noch) und der Benutzer über den Abbruch des Vorgangs informiert werden (Ausgabe einer Fehlermeldung und der Aufforderung, die Karte zu entnehmen).
   
 * * *
-  Fehlende Funktionen wurden implementiert und über den Unit Test "testPinEingabeDreimalFalsch()" getestet.
