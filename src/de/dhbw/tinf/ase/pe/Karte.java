package de.dhbw.tinf.ase.pe;

public class Karte {

	private String pin;

	public Karte(String pin) {
		try {
			Integer.parseInt(pin);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("PIN muss aus Zahlen bestehen!", e);
		}
		
		if (pin.length() != 4) {
			throw new IllegalArgumentException("PIN muss vierstellig sein!");
		}
		
		this.setPin(pin);
	}

	public boolean istKorrekt(String pin) {
		return this.getPin().equalsIgnoreCase(pin);
	}
	
	private String getPin() {
		return this.pin;
	}
	
	private void setPin(String pin) {
		this.pin = pin;
	}
}
