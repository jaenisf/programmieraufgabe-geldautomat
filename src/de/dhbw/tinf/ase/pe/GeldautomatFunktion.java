package de.dhbw.tinf.ase.pe;

public enum GeldautomatFunktion {
	Info("Info ausgeben"),
	Bestuecken("Geldautomat best�cken"),
    KarteEinschieben("Karte einschieben"),
    PinEingeben("Pin eingeben"),
    GeldAuszahlen("Geld auszahlen"),
    KarteEntnehmen("Karte entnehmen"),
    FuellstandAnzeigen("F�llstand anzeigen"),
    ProgrammBeenden("Programm beenden");
	
	private String menueelementText;
	
	private GeldautomatFunktion(String menueelementText) {
		this.menueelementText = menueelementText;
	}

	public String getMenueelementText() {
		return menueelementText;
	}
}