package de.dhbw.tinf.ase.pe.zustaende;

import java.util.Map;

import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.GeldautomatFunktion;

public interface Zustand {
	public Zustand ausfuehren(Geldautomat geldautomat, GeldautomatFunktion funktion);
	
	public Map<Integer, GeldautomatFunktion> getMenueauswahl();
}