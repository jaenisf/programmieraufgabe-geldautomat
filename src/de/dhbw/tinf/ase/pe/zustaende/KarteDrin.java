package de.dhbw.tinf.ase.pe.zustaende;

import java.util.HashMap;
import java.util.Map;

import de.dhbw.tinf.ase.pe.Anwendung;
import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.GeldautomatFunktion;

public class KarteDrin implements Zustand{
	
	private static KarteDrin instance = new KarteDrin();
	
	private KarteDrin() {}
	
	public static KarteDrin getInstance() {
		return instance;
	}

	@Override
	public Zustand ausfuehren(Geldautomat geldautomat, GeldautomatFunktion funktion) {
		switch(funktion) {
			case Info:
				Funktion.Info.ausfuehren(geldautomat);
				break;
			case PinEingeben:
				Funktion.PinEingeben.ausfuehren(geldautomat);
				
				if (geldautomat.getPinKorrekt() == true) return PinKorrekt.getInstance();
				if (geldautomat.getPinFalsch() > 2) return Bereit.getInstance();
				
				break;
			case KarteEntnehmen:	
				Funktion.KarteEntnehmen.ausfuehren(geldautomat);
				return Bereit.getInstance();
			case FuellstandAnzeigen:
				Funktion.FuellstandAnzeigen.ausfuehren(geldautomat);
				break;
			case ProgrammBeenden:
				Funktion.ProgrammBeenden.ausfuehren(geldautomat);
				break;
			default:
				break;
		}
		
		return KarteDrin.getInstance();
	}
	
	public enum Funktion {
        Info {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	geldautomat.setInfo("Falsche PIN oder PIN nicht eingegeben - " 
            						+ "Abhebung nicht m�glich!");
            }
        },
        PinEingeben {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.pinEingeben(geldautomat);
            }
        },
        KarteEntnehmen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.karteEntnehmen(geldautomat);
            }
        },
        FuellstandAnzeigen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.fuellstandAnzeigen();
            }
        },
        ProgrammBeenden {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {}
        };

        public abstract void ausfuehren(Geldautomat geldautomat);
    }

	@Override
	public Map<Integer, GeldautomatFunktion> getMenueauswahl() {
		Map<Integer, GeldautomatFunktion> men�auswahl = new HashMap<Integer, GeldautomatFunktion>();
		
		men�auswahl.put(1, GeldautomatFunktion.Info);
		men�auswahl.put(2, GeldautomatFunktion.PinEingeben);
		men�auswahl.put(3, GeldautomatFunktion.KarteEntnehmen);
		men�auswahl.put(4, GeldautomatFunktion.FuellstandAnzeigen);
		men�auswahl.put(5, GeldautomatFunktion.ProgrammBeenden);
		
		return men�auswahl;
	}
}