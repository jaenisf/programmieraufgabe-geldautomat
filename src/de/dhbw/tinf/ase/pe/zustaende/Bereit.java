package de.dhbw.tinf.ase.pe.zustaende;

import java.util.HashMap;
import java.util.Map;

import de.dhbw.tinf.ase.pe.Anwendung;
import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.GeldautomatFunktion;

public class Bereit implements Zustand{
	
	private static Bereit instance = new Bereit();
	
	private Bereit() {}
	
	public static Bereit getInstance() {
		return instance;
	}

	@Override
	public Zustand ausfuehren(Geldautomat geldautomat, GeldautomatFunktion funktion) {
		switch(funktion) {
			case Info:
				Funktion.Info.ausfuehren(geldautomat);
				break;
			case Bestuecken:
				Funktion.Bestuecken.ausfuehren(geldautomat);
				break;
			case KarteEinschieben:
				Funktion.KarteEinschieben.ausfuehren(geldautomat);
				
				if (geldautomat.getKarte() != null) return KarteDrin.getInstance();
				
				break;
			case FuellstandAnzeigen:
				Funktion.FuellstandAnzeigen.ausfuehren(geldautomat);
				break;
			case ProgrammBeenden:
				Funktion.ProgrammBeenden.ausfuehren(geldautomat);
				break;
			default:
				break;
		}
		
		return Bereit.getInstance();
	}

	private enum Funktion {
        Info {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	if (geldautomat.getFuellstand() > 500) {
        			geldautomat.setInfo("Alles OK - bitte Karte eingeben");
        		} else {
        			geldautomat.setInfo("Abhebung bis zu " 
						        		+ geldautomat.getFuellstand() 
						        		+ " Geld ist m�glich - bitte Karte eingeben");
        		}
            }
        },
        Bestuecken {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.geldautomatBestuecken(geldautomat);
            }
        },
        KarteEinschieben {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.karteEinschieben(geldautomat);
            }
        },
        FuellstandAnzeigen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.fuellstandAnzeigen();
            }
        },
        ProgrammBeenden {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {}
        };

        public abstract void ausfuehren(Geldautomat geldautomat);
    }
	
	public Map<Integer, GeldautomatFunktion> getMenueauswahl() {
		Map<Integer, GeldautomatFunktion> men�auswahl = new HashMap<Integer, GeldautomatFunktion>();
		
		men�auswahl.put(1, GeldautomatFunktion.Info);
		men�auswahl.put(2, GeldautomatFunktion.Bestuecken);
		men�auswahl.put(3, GeldautomatFunktion.KarteEinschieben);
		men�auswahl.put(4, GeldautomatFunktion.FuellstandAnzeigen);
		men�auswahl.put(5, GeldautomatFunktion.ProgrammBeenden);
		
		return men�auswahl;
	}
}