package de.dhbw.tinf.ase.pe.zustaende;

import java.util.HashMap;
import java.util.Map;

import de.dhbw.tinf.ase.pe.Anwendung;
import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.GeldautomatFunktion;

public class KeinGeld implements Zustand{
	
	private static KeinGeld instance = new KeinGeld();
	
	private KeinGeld() {}

	public static KeinGeld getInstance() {
		return instance;
	}

	@Override
	public Zustand ausfuehren(Geldautomat geldautomat, GeldautomatFunktion funktion) {
		switch(funktion) {
			case Info:
				Funktion.Info.ausfuehren(geldautomat);
				break;
			case Bestuecken:
				Funktion.Bestuecken.ausfuehren(geldautomat);
				if (geldautomat.getFuellstand() > 0) return Bereit.getInstance();
				break;
			case FuellstandAnzeigen:
				Funktion.FuellstandAnzeigen.ausfuehren(geldautomat);
				break;
			case ProgrammBeenden:
				Funktion.ProgrammBeenden.ausfuehren(geldautomat);
				break;
			default:
				break;
		}
		
		return KeinGeld.getInstance();
	}
	
	public enum Funktion {
        Info {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	geldautomat.setInfo("Der Automat enth�lt 0 Taler.");
            }
        },
        Bestuecken {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.geldautomatBestuecken(geldautomat);
            }
        },
        FuellstandAnzeigen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.fuellstandAnzeigen();
            }
        },
        ProgrammBeenden {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {}
        };

        public abstract void ausfuehren(Geldautomat geldautomat);
    }

	@Override
	public Map<Integer, GeldautomatFunktion> getMenueauswahl() {
		Map<Integer, GeldautomatFunktion> men�auswahl = new HashMap<Integer, GeldautomatFunktion>();
		
		men�auswahl.put(1, GeldautomatFunktion.Info);
		men�auswahl.put(2, GeldautomatFunktion.Bestuecken);
		men�auswahl.put(3, GeldautomatFunktion.FuellstandAnzeigen);
		men�auswahl.put(4, GeldautomatFunktion.ProgrammBeenden);
		
		return men�auswahl;
	}
}