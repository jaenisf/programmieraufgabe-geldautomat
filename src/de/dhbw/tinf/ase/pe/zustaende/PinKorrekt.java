package de.dhbw.tinf.ase.pe.zustaende;

import java.util.HashMap;
import java.util.Map;

import de.dhbw.tinf.ase.pe.Anwendung;
import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.GeldautomatFunktion;
import de.dhbw.tinf.ase.pe.zustaende.KeinGeld.Funktion;

public class PinKorrekt implements Zustand{
	
	private static PinKorrekt instance = new PinKorrekt();
	
	private PinKorrekt() {}

	public static PinKorrekt getInstance() {
		return instance;
	}

	@Override
	public Zustand ausfuehren(Geldautomat geldautomat, GeldautomatFunktion funktion) {
		switch(funktion) {
			case Info:
				Funktion.Info.ausfuehren(geldautomat);
				break;
			case GeldAuszahlen:
				Funktion.GeldAuszahlen.ausfuehren(geldautomat);
				
				if (geldautomat.getFuellstand() <= 0) return KeinGeld.getInstance();
				
				break;
			case KarteEntnehmen:
				Funktion.KarteEntnehmen.ausfuehren(geldautomat);
				return Bereit.getInstance();
			case FuellstandAnzeigen:
				Funktion.FuellstandAnzeigen.ausfuehren(geldautomat);
				break;
			case ProgrammBeenden:
				Funktion.ProgrammBeenden.ausfuehren(geldautomat);
				break;
			default:
				break;
		}
		
		return PinKorrekt.getInstance();
	}
	
	public enum Funktion {
        Info {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	if (geldautomat.getFuellstand() >= 500) {
            		geldautomat.setInfo("Maximalbetrag kann abgehoben werden");
        		} else {
        			geldautomat.setInfo("Abhebung bis zu " 
						        		+ geldautomat.getFuellstand() 
						        		+ " Geld ist m�glich");
        		}
            }
        },
        GeldAuszahlen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.geldAuszahlen(geldautomat);
            }
        },
        KarteEntnehmen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.karteEntnehmen(geldautomat);
            }
        },
        FuellstandAnzeigen {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            	Anwendung.fuellstandAnzeigen();
            }
        },
        ProgrammBeenden {
            @Override
            public void ausfuehren(Geldautomat geldautomat) {
            }
        };

        public abstract void ausfuehren(Geldautomat geldautomat);
    }

	@Override
	public Map<Integer, GeldautomatFunktion> getMenueauswahl() {
		Map<Integer, GeldautomatFunktion> men�auswahl = new HashMap<Integer, GeldautomatFunktion>();
		
		men�auswahl.put(1, GeldautomatFunktion.Info);
		men�auswahl.put(2, GeldautomatFunktion.GeldAuszahlen);
		men�auswahl.put(3, GeldautomatFunktion.KarteEntnehmen);
		men�auswahl.put(4, GeldautomatFunktion.FuellstandAnzeigen);
		men�auswahl.put(5, GeldautomatFunktion.ProgrammBeenden);
		
		return men�auswahl;
	}
}