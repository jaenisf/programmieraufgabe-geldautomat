package de.dhbw.tinf.ase.pe;

import de.dhbw.tinf.ase.pe.zustaende.KeinGeld;
import de.dhbw.tinf.ase.pe.zustaende.Zustand;

public class Geldautomat {

	private Zustand aktuellerZustand = KeinGeld.getInstance();
	
	private int fuellstand = 0;
	
	private Karte karte;
	private boolean pinKorrekt = false;
	private int pinFalsch = 0;
	
	private String info = "";
	
	public void ausfuehren(GeldautomatFunktion geldautomatFunktion) {
		this.setZustand(this.getZustand().ausfuehren(this, geldautomatFunktion));
	}
	
	public void bestuecken(int bargeld) {	
		this.setFuellstand(this.getFuellstand() + bargeld);
	}
	
	public void karteEinschieben(Karte karte) {		
		if (karte == null) {
			throw new IllegalArgumentException("Die �bergebene Karte ist gleich null!");
		}
		
		this.setPinFalsch(0);
		this.setKarte(karte);
	}

	public boolean pinEingeben(String pin) {		
		try {
			Integer.parseInt(pin);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("PIN muss aus Zahlen bestehen!",	e);
		}

		if (pin.length() != 4) {
			throw new IllegalArgumentException("PIN muss vierstellig sein1");
		}

		if (this.getKarte().istKorrekt(pin)) {
			this.setPinKorrekt(true);
		} else {
			this.setPinFalsch(this.getPinFalsch() + 1);
		}
		
		if (this.getPinFalsch() > 2) {
			this.karteEntnehmen();
			
			throw new IllegalStateException("PIN zu oft falsch eingegeben! Die Karte wurde ausgegeben!");
		}
		
		return this.getPinKorrekt();
	}

	public int geldAuszahlen(int summe) {
		if (summe < 5 || summe > 500) {
			throw new IllegalArgumentException(
					"Der Betrag muss zwischen 5 und 500 Geld liegen!");
		}
		
		if (summe % 5 != 0) {
			throw new IllegalArgumentException(
					"Der Betrag muss durch 5 teilbar sein, da nur Scheine ausgegeben werden k�nnen!");
		}

		if (this.fuellstand < summe) {
			int ausbezahlt = this.getFuellstand();
			this.setFuellstand(0);
	
			this.karteEntnehmen();
			
			return ausbezahlt;
		}
		
		this.setFuellstand(this.getFuellstand() - summe);
		
		return summe;
	}
	
	public void karteEntnehmen() {
		this.setKarte(null);
		this.setPinKorrekt(false);
	}
	
	public Zustand getZustand() {
		return this.aktuellerZustand;
	}

	private void setZustand(Zustand zustand) {
		this.aktuellerZustand = zustand;
	}
	
	public int getFuellstand() {
		return fuellstand;
	}
	
	private void setFuellstand(int fuellstand) {
		this.fuellstand = fuellstand;
	}
	
	public boolean getPinKorrekt() {
		return pinKorrekt;
	}
	
	private void setPinKorrekt(boolean pinKorrekt) {
		this.pinKorrekt = pinKorrekt;
	}
	
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	public int getPinFalsch() {
		return this.pinFalsch;
	}
	
	private void setPinFalsch(int pinFalsch) {
		this.pinFalsch = pinFalsch;
	}
	
	public Karte getKarte() {
		return this.karte;
	}
	
	private void setKarte(Karte karte) {
		this.karte = karte;
	}
}
