package de.dhbw.tinf.ase.pe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class Anwendung {
	
	private static Geldautomat geldautomat = new Geldautomat();

	private static BufferedReader cin = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) throws Exception {
		horizontaleLinie();
		System.out.println("Willkommen beim DHBW Geldautomat!");
		horizontaleLinie();

		while (true) {
			anzeigenMenueauswahl();

			String input = cin.readLine();
			
			System.out.println();

			int aktion = 0;

			try {
				aktion = Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Unzul�ssige Eingabe!");
				continue;
			}
			
			Map<Integer, GeldautomatFunktion> menueauswahl = geldautomat.getZustand().getMenueauswahl();
			
			GeldautomatFunktion geldautomatFunktion = menueauswahl.get(aktion);
			
			if (geldautomatFunktion == null) {
				System.out.println("Unzul�ssige Eingabe!");		
			} else {
				try {
					geldautomat.ausfuehren(geldautomatFunktion);
				} catch(Exception ex) {
					System.out.println(ex.getMessage());
				}
				
				if (geldautomatFunktion == GeldautomatFunktion.Info) {
					System.out.println(geldautomat.getInfo());
				}
				
				if (geldautomatFunktion == GeldautomatFunktion.ProgrammBeenden) {
					break;
				}
			}
		}

		horizontaleLinie();
		System.out.println("Danke, dass du den DHBW Geldautomat benutzt hast :-)");
		horizontaleLinie();
		
		cin.close();
	}

	public static void geldAuszahlen(Geldautomat geldautomat) {
		System.out.print("Bitte gib den gew�nschten Betrag ein: ");
		
		try {
			String input = cin.readLine();
			int abheben = Integer.parseInt(input);
			
			int summe = geldautomat.geldAuszahlen(abheben);
			
			if (summe == abheben) {
				System.out.println(summe + " Taler ausgegeben - viel Spa� damit!");
			}
			
			if (summe < abheben) {
				System.out.println("Der Geldautomat hatte nicht gen�gend Geld vorhanden! Es wurden " + summe + " Taler ausgegeben - viel Spa� damit!");
			}
			
			if (geldautomat.getFuellstand() == 0) {
				System.out.println("Der Geldautomat besitzt kein Geld mehr und ihre Karte wurde ausgegeben!");
			}
		} catch (IOException | NumberFormatException e) {
			geldAuszahlen(geldautomat);
		}
	}

	public static void pinEingeben(Geldautomat geldautomat) {
		System.out.print("Bitte gib jetzt deine PIN ein: ");
		
		try {
			String pin = cin.readLine();
			boolean pinKorrekt = geldautomat.pinEingeben(pin);
			
			if (pinKorrekt) {
				System.out.println("Die PIN wurde korrekt eingegeben.");
			} else {
				System.out.println("Die PIN wurde falsch eingegeben! Bitte versuchen Sie es noch einmal!");
			}
		} catch (IOException e) {
			pinEingeben(geldautomat);
		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void karteEinschieben(Geldautomat geldautomat) {
		String pin = erzeugePin();
		System.out.println("Die Pin f�r deine Karte ist " + pin + "!");
		
		Karte karte = new Karte(pin);
		geldautomat.karteEinschieben(karte);
		System.out.println("Die Karte ist jetzt im Automat!");
	}
	
	public static void karteEntnehmen(Geldautomat geldautomat) {
		geldautomat.karteEntnehmen();
		System.out.println("Deine Karte wurde wieder ausgeworfen!");
	}
	
	private static String erzeugePin() {
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < 4; i++) {
			double zufall = Math.random();
			double ziffer = zufall;
			ziffer *= 10;
			sb.append((int) ziffer);
		}
		
		return sb.toString();
	}

	public static void geldautomatBestuecken(Geldautomat geldautomat) {
		System.out.print("Bitte gib die Summe ein: ");
		
		try {
			String input = cin.readLine();
			int summe = Integer.parseInt(input);
			geldautomat.bestuecken(summe);
		} catch (NumberFormatException | IOException e) {
			geldautomatBestuecken(geldautomat);
		}
	}
	
	public static void fuellstandAnzeigen() {
		System.out.println("Der Automat enth�lt " + geldautomat.getFuellstand() + " Taler!");
	}

	private static void anzeigenMenueauswahl() {
		System.out.println();
		horizontaleLinie();
		System.out.println("Was willst du tun?");
		
		Map<Integer, GeldautomatFunktion> menueauswahl = geldautomat.getZustand().getMenueauswahl();
		
		for (Map.Entry<Integer, GeldautomatFunktion> menueeintrag : menueauswahl.entrySet()) {
			System.out.println(menueeintrag.getKey() + " - " + menueeintrag.getValue().getMenueelementText());
		}
		
		horizontaleLinie();
	}
	
	private static void horizontaleLinie() {
		horizontaleLinie(53);
	}
	
	private static void horizontaleLinie(int laenge) {
		String s = "";
		
		for(int i = 1; i <= laenge; i++) {
			s += "\u2014";
		}
		
		System.out.println(s);
	}
}