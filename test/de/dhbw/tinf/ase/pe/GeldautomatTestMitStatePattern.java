package de.dhbw.tinf.ase.pe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.zustaende.Bereit;
import de.dhbw.tinf.ase.pe.zustaende.KarteDrin;
import de.dhbw.tinf.ase.pe.zustaende.KeinGeld;
import de.dhbw.tinf.ase.pe.zustaende.Zustand;

public class GeldautomatTestMitStatePattern {

	@Test (expected = IllegalArgumentException.class)
	public void testKarteIstNull() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(null);
	}
	
	@Test
	public void testZweiteKarteEinschieben() {
		assertFalse(KarteDrin.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.KarteEinschieben));
	}

	/*@Test wird nicht mehr ben�tigt, da Geld auszahlen nicht mehr im Zustand KarteDrin aufgerufen werden kann: siehe testAbhebungOhnePin()
	public void testFalschePin() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.einschieben(new Karte("1111"));
		geldautomat.pinEingeben("2222");
		int summe = geldautomat.auszahlen(500);
		assertEquals("Bei falscher PIN muss -1 zur�ckgegeben werden!", -1, summe);
	}*/
	
	@Test
	public void testAbhebungOhnePin() {
		assertFalse(KarteDrin.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.GeldAuszahlen));
	}

	@Test
	public void testBestueckung() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		assertEquals("Bestand muss �bereinstimmen!", 100, geldautomat.getFuellstand());
	}
	
	@Test
	public void testZuWenigGeldImAutomat() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		int summe = geldautomat.geldAuszahlen(300);
		assertEquals("Bei zu wenig Geld muss der Rest(100) zur�ckgegeben werden!", 100, summe);
		assertEquals("Bargeld muss jetzt 0 sein!", 0, geldautomat.getFuellstand());
	}
	
	//-----------------------------------------------------------------------------------------
	
	//Unit Test Erg�nzung
	
	@Test 
	public void testGeldAutomatStartetOhneGeld() {
		Geldautomat geldautomat = new Geldautomat();
		assertEquals(0, geldautomat.getFuellstand());
		assertEquals(KeinGeld.class, geldautomat.getZustand().getClass());
	}
	
	@Test
	public void testKarteEinlesenInLeerenAutomaten() {
		assertFalse(KeinGeld.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.KarteEinschieben));
	}
	
	@Test
	public void testBest�ckungMitEingeschobenerKarte() {
		assertFalse(KarteDrin.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.Bestuecken));
	}
	
	/* wird nicht mehr ben�tigt, da Geld auszahlen nicht mehr im Zustand KarteDrin aufgerufen werden kann: siehe testAbhebungOhnePin()
	@Test 
	public void testAbhebungOhnePinMitMehrGeldAlsVorhanden() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.einschieben(new Karte("1111"));
		int summe = geldautomat.auszahlen(300);
		assertEquals("Bei fehlender PIN muss -1 zur�ckgegeben werden!", -1, summe);
	}*/
	
	@Test
	public void testZustandswechselBeiKorrekterPinEingabe() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		assertEquals("Der Zustand muss zu Pin Korrekt wechseln bei korrekter Pin Eingabe!",
				true, geldautomat.getPinKorrekt());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonZuVielGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(600);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonZuWenigGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonNichtDurch5TeilbarenBetr�gen() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(264);
	}
	
	@Test
	public void testAbhebenVonGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		int summe = geldautomat.geldAuszahlen(300);
		assertEquals("Der abgehobene Betrag muss bei vorhandenem Geld dem beantragten Betrag �bereinstimmen.",
				300, summe);
	}
	
	@Test
	public void testPinKorrekt() {
		Karte karte = new Karte("1243");
		boolean istKorrekt = karte.istKorrekt("1243");
		assertEquals("Die Pin muss �bereinstimmen!", true, istKorrekt);
	}
	
	@Test
	public void testPinNichtKorrekt() {
		Karte karte = new Karte("1243");
		boolean istKorrekt = karte.istKorrekt("3425");
		assertEquals("Die Pins stimmen nicht �berein! Die Ausgabe muss 'false' sein!", false, istKorrekt);
	}
	
	@Test
	public void testPinKorrektMitF�hrendenNullen() {
		Karte karte = new Karte("0043");
		boolean istKorrekt = karte.istKorrekt("0043");
		assertEquals("Die Pin muss auch mit f�hrenden Nullen �bereinstimmen!", true, istKorrekt);
	}
	
	@Test
	public void testPinEingabeOhneVorhandeneKarte() {
		assertFalse(Bereit.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.PinEingeben));
		assertFalse(KeinGeld.getInstance().getMenueauswahl().containsValue(GeldautomatFunktion.PinEingeben));
	}
	
	@Test (expected = IllegalStateException.class)
	public void testPinEingabeDreimalFalsch() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("3333");
		geldautomat.pinEingeben("4444");
		geldautomat.pinEingeben("5555");
		assertNull(geldautomat.getKarte());
	}
}
