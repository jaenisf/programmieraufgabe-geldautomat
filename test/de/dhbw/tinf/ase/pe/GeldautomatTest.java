package de.dhbw.tinf.ase.pe;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.tinf.ase.pe.Geldautomat;
import de.dhbw.tinf.ase.pe.Karte;
import junit.framework.Assert;

public class GeldautomatTest {

	@Test (expected = IllegalArgumentException.class)
	public void testKarteIstNull() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(null);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testZweiteKarteEinschieben() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.karteEinschieben(new Karte("2222"));
	}

	@Test
	public void testFalschePin() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("2222");
		int summe = geldautomat.geldAuszahlen(500);
		assertEquals("Bei falscher PIN muss -1 zurückgegeben werden!", -1, summe);
	}
	
	@Test
	public void testAbhebungOhnePin() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		int summe = geldautomat.geldAuszahlen(500);
		assertEquals("Bei falscher PIN muss -1 zurückgegeben werden!", -1, summe);
	}

	@Test
	public void testBestueckung() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		assertEquals("Bestand muss Übereinstimmen!", 100, geldautomat.getFuellstand());
	}
	
	@Test
	public void testZuWenigGeldImAutomat() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		int summe = geldautomat.geldAuszahlen(300);
		assertEquals("Bei zu wenig Geld muss der Rest(100) zurückgegeben werden!", 100, summe);
		assertEquals("Bargeld muss jetzt 0 sein!", 0, geldautomat.getFuellstand());
	}
	
	//-----------------------------------------------------------------------------------------
	
	//Unit Test Ergänzung
	
	@Test 
	public void testGeldAutomatStartetOhneGeld() {
		Geldautomat geldautomat = new Geldautomat();
		assertEquals(0, geldautomat.getFuellstand());
	}
	
	@Test (expected = IllegalStateException.class)
	public void testKarteEinlesenInLeerenAutomaten() {
		Geldautomat geldautomat = new Geldautomat();
		Karte karte = new Karte("1234");
		geldautomat.karteEinschieben(karte);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testBestückungMitEingeschobenerKarte() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		Karte karte = new Karte("1234");
		geldautomat.karteEinschieben(karte);
		geldautomat.bestuecken(100);
	}
	
	@Test
	public void testAbhebungOhnePinMitMehrGeldAlsVorhanden() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.karteEinschieben(new Karte("1111"));
		int summe = geldautomat.geldAuszahlen(300);
		assertEquals("Bei fehlender PIN muss -1 zurückgegeben werden!", -1, summe);
	}
	
	@Test
	public void testZustandswechselBeiKorrekterPinEingabe() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(100);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		assertEquals("Der Zustand muss zu Pin Korrekt wechseln bei korrekter Pin Eingabe!",
				true, geldautomat.getPinKorrekt());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonZuVielGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(600);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonZuWenigGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(0);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAbhebenVonNichtDurch5TeilbarenBeträgen() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		geldautomat.geldAuszahlen(264);
	}
	
	@Test
	public void testAbhebenVonGeld() {
		Geldautomat geldautomat = new Geldautomat();
		geldautomat.bestuecken(1000);
		geldautomat.karteEinschieben(new Karte("1111"));
		geldautomat.pinEingeben("1111");
		int summe = geldautomat.geldAuszahlen(300);
		assertEquals("Der abgehobene Betrag muss bei vorhandenem Geld dem beantragten Betrag übereinstimmen.",
				300, summe);
	}
	
	@Test
	public void testPinKorrekt() {
		Karte karte = new Karte("1243");
		boolean istKorrekt = karte.istKorrekt("1243");
		assertEquals("Die Pin muss übereinstimmen!", true, istKorrekt);
	}
	
	@Test
	public void testPinNichtKorrekt() {
		Karte karte = new Karte("1243");
		boolean istKorrekt = karte.istKorrekt("3425");
		assertEquals("Die Pins stimmen nicht überein! Die Ausgabe muss 'false' sein!", false, istKorrekt);
	}
	
	@Test
	public void testPinKorrektMitFührendenNullen() {
		Karte karte = new Karte("0043");
		boolean istKorrekt = karte.istKorrekt("0043");
		assertEquals("Die Pin muss auch mit führenden Nullen übereinstimmen!", true, istKorrekt);
	}
	
	@Test (expected = IllegalStateException.class)
	public void testPinEingabeOhneVorhandeneKarte() {
		Geldautomat geldautomat = new Geldautomat();
		Karte karte = new Karte("0043");
		geldautomat.bestuecken(100);
		geldautomat.pinEingeben("0043");
	}
}
